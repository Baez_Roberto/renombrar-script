import org.apache.commons.io.FileUtils;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by c00baezmr on 03/12/2018.
 */



public class Main extends JFrame implements ActionListener {

    private JTextField textfield1,textfield2,textfield3;
    private JLabel label1,label2,label3;
    private JButton boton1;

    public Main() {

        setLayout(null);
        label1=new JLabel("Ruta Scripts:");
        label1.setBounds(10,10,100,10);
        add(label1);
        label2=new JLabel("Segundo valor Version");
        label2.setBounds(10,50,250,30);
        add(label2);

        label3=new JLabel("Tercer valor Version");
        label3.setBounds(10,50,250,130);
        add(label3);



        textfield1=new JTextField();
        textfield1.setBounds(150,0,300,30);
        add(textfield1);

        textfield2=new JTextField();
        textfield2.setBounds(150,50,50,30);
        add(textfield2);

        textfield3=new JTextField();
        textfield3.setBounds(150,100,50,30);
        add(textfield3);

        boton1=new JButton("Aceptar");
        boton1.setBounds(130,150,150,50);
        add(boton1);
        boton1.addActionListener(this);
    }

    public Main(String[] archivosVersionados) {
        setLayout(null);
        label1=new JLabel("Se versionaron:" + archivosVersionados.length + " script/s");
        label1.setBounds(20,20,300,100);
        add(label1);

    }

    public void actionPerformed(ActionEvent e){
        if (e.getSource()==boton1) {
            String valorFijoVersionado = textfield2.getText();
            String ultimoValorVersionado = textfield3.getText();
            String ruta = textfield1.getText();


            File directorio = new File(ruta);
            String[] ficheros = directorio.list();
            String[] archivosVersionados = directorio.list();

            for (int x = 0; x < ficheros.length; x++) {

                File archivo1 = new File(ruta + "\\" + ficheros[x]);
                File archivo2 = new File(ruta + "\\V1_" + valorFijoVersionado + "_" + ultimoValorVersionado + "__" + ficheros[x]);
                archivo1.renameTo(archivo2);
                String content = null;
                try {
                    content = FileUtils.readFileToString(archivo2, "Windows-1252");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                //FileUtils.write(archivo1, content, "UTF-8");
                try {
                    FileUtils.write(archivo2, content, "UTF-8");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                archivosVersionados[x] = archivo2.getName();
            }

            Main ventanaAviso = new Main(archivosVersionados);
            ventanaAviso.setBounds(30,30,300,150);
            ventanaAviso.setTitle("Resultado");
            ventanaAviso.setVisible(true);
        }

    }

    public static void main(String[] args) throws IOException {

        Main formulario1=new Main();
        formulario1.setBounds(0,0,500,300);
        formulario1.setTitle("Renombrador de Scripts");
        formulario1.setVisible(true);
        formulario1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
